import numpy as np
import read_graph as rg
import graph_tool.inference as gti


def read_block_partition(file_name_block_list_start,file_name_block_list_end,no_hier_levels = 1):
	"""
	Reads in block partition files for SBM construction.
	It relies on partition files in which the partition indices of each node will be given, e.g. as generated with write_fit_to_file.py. 
	For the file naming, it is required that there is a running index 
	(on the hierarchy level), starting from zero (lowest hierarchy level). The function output can be used to initiate an SBM with the according partition.
	
	:Parameters:
	
	- file_name_block_list_start: beginning of filename before running index
	- file_name_block_list_end: end of filename after running index
	- no_hier_levels: number of hierarchy levels, defaults to 1 (planar SBM)
	
	:Returns:
	
	list of np arrays which can used as partition input for SBM building
	
	:Example:
	
	Read in a hierarchical partition as given in the folder example_files::

		import read_SBM as rsbm
		block_prot = rsbm.read_block_partition("example_files/metabbud_erneg_sf_hSBM_bestfit_block_lev",
			".txt",no_hier_levels = 3)

	"""

#	initialize  data objects
	block_file=[]
#
	for i in range(no_hier_levels):
		temp_block_file=[]
		with open(file_name_block_list_start+str(i)+file_name_block_list_end, 'r') as thefile:
			for line in thefile:
				temp_block_file.append(int(line.strip()))
#
		block_file.append(np.asarray(temp_block_file))
#
	return(block_file)
	

def read_SBM(edgelist_filename,total_node_count,file_name_block_list_start,file_name_block_list_end,no_hier_levels = 1,
	is_hierarchical=True,is_degree_corrected=True):
	"""
	Create an SBM from a graph given by an edge list and the (hierarchical) partitions given in the files.

	No additional graph characteristics are implemented, i.e. no edge or node property maps.
	
	:Parameters:
	
	- edgelist_filename: name of the edgelist of the underlying graph
	- total_node_count: number of nodes which should be in the graph (disconnected nodes are added)
	- file_name_block_list_start: beginning of filename before running index (assumed to start with 0)
	- file_name_block_list_end: end of filename after running index
	- no_hier_levels: number of hierarchy levels, defaults to 1 (planar SBM)
	- is_hierarchical: whether the hierarchical or planar SBM is to be established
	- is_degree_corrected: whether the SBM is degree-corrected (default) or not
	
	:Returns:
	
	A graph_tool::NestedBlockState or graph_tool::BlockState object with the graph from the edge list as underlying graph and the partition as given from the files.
	
	:Example:
	
	Using the example files to create an SBM::

		import read_SBM as rsbm
		sbm_metab = read_SBM("example_files/180905_metabbud_erneg_spear_scalefree_el.csv",
			162,
			"example_files/metabbud_erneg_sf_hSBM_bestfit_block_lev",
			".txt",
			no_hier_levels = 3,
			is_hierarchical=True,
			is_degree_corrected=False)

	"""

	#initialize graph from edgelist and adding a certain number of disconnected nodes to reach target_node_count nodes
	g0,ew=rg.read_graph(edgelist_filename,total_node_count)
	#read in partition of blocks from files
	block_prot = read_block_partition(file_name_block_list_start,file_name_block_list_end, no_hier_levels)
	#create SBM
	if is_hierarchical:
		
		sbm_model = gti.NestedBlockState(g=g0,bs=block_prot,sampling=True,deg_corr=is_degree_corrected)
	
	else: 
		
		sbm_model = gti.BlockState(g=g0,b=block_prot,deg_corr=is_degree_corrected)
	#

	return(sbm_model)
