import graph_tool as gt
import global_fit as gf
import write_fit_to_file as wf
import read_graph as rg
import sys

def main():
    el_name =sys.argv[1] #name of edge list to import, given by shell script
    total_node_count=sys.argv[2] #number of total nodes the network should have
    fitno = sys.argv[3] #number of fits to initialize
    out_name = sys.argv[4] #name of the output file, given by shell script in order to write to distinct files
    iterno = sys.argv[5] #use this as addition to the filename to accommodate calling this function (e.g. in a bash loop) multiple times

    #gt.openmp_set_num_threads(1) #use only one core at a time: How many processes are leapt by Python's graph-tool 
    #Option only accessible if openMP was enabled during compilation of graph_tool.
    #This needs to be especially well controlled for computations in HPC environments. 

    graph_ini,ew = rg.read_graph(el_name,int(total_node_count)) #read edge list and establish network, add nodes to reach target node count

    a1,b1,c1 = gf.global_fit(graph_ini,random_ini_no=int(fitno),return_fit_no=int(fitno), #fitno are fitted and reported
    model_opt={'dc_SBM':True,'hierarchical_SBM':True, 'overlap_SBM':False, 'weighted_SBM':False},
    fit_method_opt={'method_string':'best_state'}) #no MCMC

    wf.write_fit_to_file(a1,b1,c1,None,out_name+iterno,blockno_opt={'export_blockno':int(fitno),'hierarchical_SBM':True}) #export all fitted block partitions


if __name__ == '__main__': #call the script if this calls the function, not if only loaded
    main()