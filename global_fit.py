import graph_tool as gt
import graph_tool.inference as gti

def global_fit(ini_graph,random_ini_no,return_fit_no,model_opt,fit_method_opt):
	"""
	Perform a global fit of the initial graph on a certain SBM version. Wrapper for minimize_nested_blockmodel() and minimize_blockmodel().


	:Parameters:
	
	- ini_graph: initial graph to fit to SBM, Graph object, assumes that 'edge_weight' exists as edge property which
	  can be fitted to
	- random_ini_no: (optional) number of random initiations of the fit, default: 1
	- return_fit_no: (optional) number of best fits to return, default: 1
	- model_opt: (optional) Options for the SBM to use

			- 'dc_SBM' - Boolean, True [default] for degree corrected SBM
			- 'hierarchical_SBM' - Boolean, default False (planar, non-nested model), True for hierarchical (nested) SBM
			- 'overlap_SBM' - Boolean, default False, True if overlap should be computed 
			- 'weighted_SBM' - Boolean, default False, True for weighted. NOTE: only the edge property 'edge_weight' is going to be incorporated as edge covariate
			- 'weight_prior': mandatory if 'weighted_SBM' == True, default: 'real-normal'; prior distribution assumed for the edge weight covariate, should be one of 
				
				- 'real-exponential' 
				- 'real-normal' 
				- 'discrete-geometric'
				- 'discrete-binomial'
				- 'discrete-poisson'
	
	- fit_method_opt: (optional) options for the global fit

			- 'method_string' 

				- 'best_state' [default] - report the x best fits (lowest minimal description length, default graph-tool parameters), or 
				- 'with_mcmc' - also perform an additional mcmc-equilibration for the best fits (not supported for nested overlapping SBM)

	  		- 'mcmc_fit_no' - integer giving the number of fits for which to equilibrate via MCMC sampling, default: return_fit_no
			- 'mcmc_niter' - number of iterations, default: 10
			- 'mcmc_nbreaks' - number of record-breaking events, default: 2
			- 'mcmc_wait' - number of cycles to wait without record-breaking event, default: 1000
	
	:Returns:

	- Three objects if fit_method_opt['method_string']=='best_state':

		- optimal minimal description lengths as list, number according to return_fit_no (not sorted!)
		- block state partition for these minimal description lengths, top_block_no (in the same order as the first return value)
		- all minimal description lengths in order of computation, number: random_ini_no

	- Four objects if fit_method_opt['method_string']=='with_mcmc':
	
		- optimal minimal description lengths as list, number according to return_fit_no (not sorted!)
		- block state partition for these minimal description lengths, top_block_no (in the same order as the first return value)
		- all minimal description lengths (sorted) before applying MCMC sampling anew, number: random_ini_no
		- all minimal description lengths after the MCMC sampling, number: mcmc_fit_no (if this is not specified, return_fit_no)
	
	:Examples:
	
	Perform the model fit to a degree-corrected, hierarchical, non-overlapping, non-weighted SBM for 10 initial 
	fits and return the partitions for the 2 best fits. Without additional MCMC fitting, we have 3 return values 
	(best 2 minimal description lengths (not sorted!), partitions for the best 2 fits, minimal description lengths 
	from all 5 initialized fits)::
	
		#import the initial graph
		import read_graph as rg
		graph_ini,ew = rg.read_graph('180905_metabbud_erneg_spear_scalefree_el.csv',total_node_count = 162)
		#perform fit
		import global_fit as gf
		top_mindl0, opt_part0, all_mindl0 = gf.global_fit(graph_ini,random_ini_no = 5,return_fit_no = 2,
			model_opt={'dc_SBM':True,'hierarchical_SBM':True, 'overlap_SBM':False, 'weighted_SBM':False},
			fit_method_opt={'method_string':'best_state'})

	Second example: Model fit to non-degree-corrected, non-hierarchical, non-overlapping, weighted SBM 
	(with real-normal prior) for 6 initial fits, performs additional MCMC sampling (with default settings) 
	for the 4 best fits, and returns the partitions for the 2 best fits. With this additional MCMC sampling, 
	we have 4 return values - the three from the first example plus the minimal description lengths from the 
	4 additional MCMC samplings::

		top_mindl1, opt_part1, all_mindl1, mcmc_mindl1 = gf.global_fit(
			graph_ini,random_ini_no = 6,return_fit_no = 2,
			model_opt={'dc_SBM':False,'hierarchical_SBM':False, 'overlap_SBM':False, 'weighted_SBM':True,'weight_prior':'real-normal'},
			fit_method_opt={'method_string':'with_mcmc','mcmc_fit_no':4})

	"""
		
#initialize the first variables
	if random_ini_no == None:
		random_ini_no=1
	if return_fit_no == None:
		return_fit_no=1

#initialize the model options	
	if model_opt==None:
		model_opt={'dc_SBM':True, 'hierarchical_SBM': False, 'overlap_SBM':False, 'weighted_SBM':False}
	else:
		if not 'dc_SBM' in model_opt:
			model_opt['dc_SBM']=True
		if not 'hierarchical_SBM' in model_opt:
			model_opt['hierarchical_SBM']=False
		if not 'overlap_SBM' in model_opt:
			model_opt['overlap_SBM']=False
		if not 'weighted_SBM' in model_opt:
			model_opt['weighted_SBM']=False
		elif model_opt['weighted_SBM']:
			weight_prior=model_opt['weight_prior'] #this has to be defined if weighted SBM is used

#initialize the fitting method options
	if fit_method_opt == None:
		fit_method_opt={'method_string':'best_state', 'mcmc_fit_no':return_fit_no,'mcmc_niter':10,'mcmc_nbreaks':2, 'mcmc_wait':1000}
	else:
		if not 'method_string' in fit_method_opt:
			fit_method_opt['method_string']='best_state'
		if not 'mcmc_fit_no' in fit_method_opt:
			fit_method_opt['mcmc_fit_no']=return_fit_no
		if not 'mcmc_niter' in fit_method_opt:
			fit_method_opt['mcmc_niter']=10
		if not 'mcmc_nbreaks' in fit_method_opt:
			fit_method_opt['mcmc_nbreaks']=2
		if not 'mcmc_wait' in fit_method_opt:
			fit_method_opt['mcmc_wait']=1000

	min_dl = [] #save all minimal description length
	top_min_dl= []  #save only best dls
	top_block_no = [] #only saving the BlockStateobjects in terms of their partitions is less memory-consuming
#	
	#if we have more fits for mcmc than are initialized, we have to adjust
	max_ini_best_no=return_fit_no
	if fit_method_opt['method_string']=='with_mcmc':
		fit_method_opt['mcmc_fit_no'] = min(random_ini_no,fit_method_opt['mcmc_fit_no']) #in order not to do more mcmcs than initialized
		max_ini_best_no = max(return_fit_no, fit_method_opt['mcmc_fit_no']) # to enable more "best" returns initially than finally
#	
	for i in range(random_ini_no):
		#fit SBM to the graph with parameters as given in 
		if model_opt['weighted_SBM'] & model_opt['hierarchical_SBM']:
			fitted_state = gti.minimize_nested_blockmodel_dl(ini_graph, deg_corr=model_opt['dc_SBM'], 
				overlap=model_opt['overlap_SBM'], 
				state_args=dict(recs=[ini_graph.ep.edge_weight],
					rec_types=[weight_prior]))

		elif model_opt['weighted_SBM'] & (not model_opt['hierarchical_SBM']):
			fitted_state = gti.minimize_blockmodel_dl(ini_graph, deg_corr=model_opt['dc_SBM'], 
				overlap=model_opt['overlap_SBM'],
				state_args=dict(recs=[ini_graph.ep.edge_weight],
					rec_types=[weight_prior]))

		elif (not model_opt['weighted_SBM']) & model_opt['hierarchical_SBM']:
			fitted_state = gti.minimize_nested_blockmodel_dl(ini_graph,deg_corr=model_opt['dc_SBM'], 
				overlap=model_opt['overlap_SBM'])

		elif (not model_opt['weighted_SBM']) & (not model_opt['hierarchical_SBM']):
			fitted_state = gti.minimize_blockmodel_dl(ini_graph,deg_corr=model_opt['dc_SBM'], 
				overlap=model_opt['overlap_SBM'])
#	
		#save the minimal description length
		min_dl.append(fitted_state.entropy())
		#save the top best min_dls and blockstate partitions
		if i<max_ini_best_no: #max_ini_best_no will only be>0 if 'with_mcmc', it enables to do more mcmcs than returning
			top_min_dl.append(min_dl[i])
			#save the partitions
			if model_opt['hierarchical_SBM']:
				if model_opt['overlap_SBM']:#only the lowest level is an OverlapBlockState
					bn1=[fitted_state.levels[0].get_majority_block().get_array()]
					for j in range(len(fitted_state.levels)-1):
						bn1.append(fitted_state.levels[j+1].get_blocks().get_array())
					top_block_no.append(bn1)
				else:
					top_block_no.append(fitted_state.get_bs()) 
			else:
				if model_opt['overlap_SBM']:
					top_block_no.append(fitted_state.get_majority_blocks().get_array())
				else:
					top_block_no.append(fitted_state.get_blocks().get_array()) #get block assignments as array as in get_bs()
		
		elif min_dl[i]<max(top_min_dl): #if the partition has a smaller dl than the top best, replace the worst
			minind=top_min_dl.index(max(top_min_dl))
			top_min_dl[minind]=min_dl[i]
			if model_opt['hierarchical_SBM']:
				if model_opt['overlap_SBM']:#only the lowest level is an OverlapBlockState
					bn1=[fitted_state.levels[0].get_majority_block().get_array()]
					for j in range(len(fitted_state.levels)-1):
						bn1.append(fitted_state.levels[j+1].get_blocks().get_array())
					top_block_no[minind]=bn1
				else:
					top_block_no[minind]=fitted_state.get_bs()
			else:
				if model_opt['overlap_SBM']:
					top_block_no[minind]=fitted_state.get_majority_blocks().get_array()
				else:
					top_block_no[minind]=fitted_state.get_blocks().get_array()		
#
	if fit_method_opt['method_string']=='best_state':
		return top_min_dl, top_block_no, min_dl
#
	#perform mcmc equilibration for the best x fits: Note - despite the fact that minimize_nested_blockmodel_dl()
	#already performs and mcmc equlibration, doing this on the result of the best fit still reduces the minimal 
	#description length
	elif fit_method_opt['method_string']=='with_mcmc':
		mcmc_min_dl =[]
		mcmc_top_min_dl=[]
		mcmc_top_block_no =[]
		#sort the top best mdls
		sorted_min_dl=sorted(top_min_dl)
		for i in range(fit_method_opt['mcmc_fit_no']):
			run_ind = top_min_dl.index(sorted_min_dl[i]) #define the lowest - for those mcmc

			#initialize the State object on which to run MCMC (this is not possible for nested overlapping blockmodel!)
			if model_opt['hierarchical_SBM']:
				#overlap_type=type(fitted_state.levels[0]) #overlapping does not work with giving the initialization like that
				
				if model_opt['weighted_SBM']:
					fitted_state=gti.NestedBlockState(ini_graph,bs=top_block_no[run_ind],deg_corr=model_opt['dc_SBM'],
						recs=[ini_graph.ep.edge_weight],rec_types=[weight_prior],sampling=True)
				
				if (not model_opt['weighted_SBM']):
					fitted_state=gti.NestedBlockState(ini_graph,bs=top_block_no[run_ind],deg_corr=model_opt['dc_SBM'],
						sampling=True)
			
			else:
				
				if model_opt['weighted_SBM'] & model_opt['overlap_SBM']:
					fitted_state=gti.OverlapBlockState(ini_graph,b=ini_graph.new_vertex_property('int',top_block_no[run_ind]),
						recs=[ini_graph.ep.edge_weight],rec_types=[weight_prior],deg_corr=model_opt['dc_SBM'])
				
				if model_opt['weighted_SBM'] & (not model_opt['overlap_SBM']):
					fitted_state=gti.BlockState(ini_graph,b=top_block_no[run_ind],recs=[ini_graph.ep.edge_weight],
						rec_types=[weight_prior],deg_corr=model_opt['dc_SBM'])
				
				if (not model_opt['weighted_SBM']) & model_opt['overlap_SBM']:
					fitted_state=gti.OverlapBlockState(ini_graph,b=ini_graph.new_vertex_property('int',top_block_no[run_ind]),
						deg_corr=model_opt['dc_SBM'])
				
				if (not model_opt['weighted_SBM']) & (not model_opt['overlap_SBM']):
					fitted_state=gti.BlockState(ini_graph,b=top_block_no[run_ind],deg_corr=model_opt['dc_SBM'])

				
			#perform mcmc equilibration
			gti.mcmc_equilibrate(fitted_state,wait=fit_method_opt['mcmc_wait'], #default graph_tool: 100
				nbreaks=fit_method_opt['mcmc_nbreaks'], #default graph_tool: 2
				mcmc_args=dict(niter=fit_method_opt['mcmc_niter']), #default graph-tool: 1 (better though: 10)
				verbose=False) #set verbose to True to see what happens
#
			mcmc_min_dl.append(fitted_state.entropy())
#			
			if i<return_fit_no:
				mcmc_top_min_dl.append(mcmc_min_dl[i])
				if model_opt['hierarchical_SBM']:
				#	if model_opt['overlap_SBM']:
				#		bn1=[fitted_state.levels[0].get_majority_blocks().get_array()]
				#		for j in range(len(fitted_state.levels)):
				#			bn1.append(fitted_state.levels[j+1].get_blocks().get_array())
				#		mcmc_top_block_no.append(bn1)
				#	else:
					mcmc_top_block_no.append(fitted_state.get_bs())
				else:
					if model_opt['overlap_SBM']:
						mcmc_top_block_no.append(fitted_state.get_majority_blocks().get_array())
					else:
						mcmc_top_block_no.append(fitted_state.get_blocks().get_array())
			#		
			elif mcmc_min_dl[i]<max(mcmc_top_min_dl):
				minind=mcmc_top_min_dl.index(max(mcmc_top_min_dl))
				mcmc_top_min_dl[minind]=mcmc_min_dl[i]
				if model_opt['hierarchical_SBM']:
					#if model_opt['overlap_SBM']:
					#	bn1=[fitted_state.levels[0].get_majority_blocks().get_array()]
					#	for j in range(len(fitted_state.levels)):
					#		bn1.append(fitted_state.levels[j+1].get_blocks().get_array())
					#	mcmc_top_block_no[minind]=bn1
					#else:
					mcmc_top_block_no[minind]=fitted_state.get_bs()
				else:
					if model_opt['overlap_SBM']:
						mcmc_top_block_no[minind].append(fitted_state.get_majority_blocks().get_array())
					else:
						mcmc_top_block_no[minind]=fitted_state.get_blocks().get_array()
				
		return mcmc_top_min_dl, mcmc_top_block_no, sorted(min_dl), mcmc_min_dl
#

			
