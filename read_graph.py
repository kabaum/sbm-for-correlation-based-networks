import graph_tool as gt
import numpy as np


def read_graph(edge_list_file_name, total_node_count=None):
	"""
	Establish a graph from an edge list including edge covariates.

	The filename is used as input. In .csv files, a column 'edge_weight' is converted to float covariate. 
	All edge covariates not named 'edge_weight' are assigned string datatypes. 
	Nodes without edges, i.e. not ocurring in the edge list, are added (as last nodes) such that the node
	count of the graph coincides with total_node_count.
	If having .xml/.graphml/.gt as input, the file is just loaded as network and nodes without edges are added 
	such that the number of nodes is correct.
	Note: We do not allow to have commas, spaces or '' within the edge property names. Also, no edge property should be named edge_index
	as this interferes with the corresponding graph-tool in-built edge property.
	
	:Parameters:
	
	- edge_list_file_name: a string giving the location of the file including its extension
	- total_node_count: a positive integer giving the number of nodes the network should have. Omit or set to None 
	  if no nodes without edges should be added
	
	:Returns:
	
	- the network as Graph object
	- a Boolean value indicating whether there is an edge_weight edge property
	
	:Example:
	
	Create a graph from the edge list provided in the example_files folder::
	
		import read_graph as rg
		graph_ini,ew = rg.read_graph('example_files/180905_metabbud_erneg_spear_scalefree_el.csv',total_node_count = 162)
	
	"""

	filename_extension = edge_list_file_name.split('.')[-1]
	#
	if filename_extension not in ["xml","graphml","gt","dot","gml","csv"]:
		print("""Cannot handle input different from ["xml","graphml","gt","dot","gml","csv"], program ends.""")
		return [],False
#
#
	if filename_extension in ["xml","graphml","gt","dot","gml"]:
		loaded_network=gt.load_graph(edge_list_file_name, fmt=filename_extension)
		# add nodes to reach required node count
		if total_node_count is not None:
			loaded_network.add_vertex(total_node_count-loaded_network.num_vertices())
#		
		if "edge_weight" in loaded_network.g.edge_properties:
			return loaded_network, True
		else:
			return loaded_network, False
#
#
	if filename_extension == "csv":
	#find out which column is the edge_weight column, read the first line of the file for that
	#in Python 2, we would have to handle the following line differently (no encoding parameter)
		with open(edge_list_file_name, 'r', encoding='utf-8') as f:
			covariate_names = f.readline().replace('"','').strip()
			#the strip() removes also the newline at the end
#
		#now separate the edge properties at the commas (note that we need a csv file as input!)
		covariate_names = covariate_names.split(',')
		eprop_names_list = covariate_names[2:] #only use the last entries
		#prepare correct entry string for eprop_types and eprop_names
		eprop_types_list = ["string"] * (len(covariate_names)-2) #first overall list
#		
		try: 
#		    
			if "edge_weight" in covariate_names: #find the edge_weight column
				ew_index = covariate_names.index("edge_weight") #if this is not in
				eprop_types_list[ew_index-2]="float" #change the entry of the edge weight 
				#we have to enter -2 because also
				#the source and target nodes will occur as columns 
#			    
			loaded_network=gt.load_graph_from_csv(edge_list_file_name,
			directed=False,
			skip_first=True,
			eprop_types=eprop_types_list,
			eprop_names=eprop_names_list)

			if total_node_count is not None:
				loaded_network.add_vertex(total_node_count-loaded_network.num_vertices())

			return loaded_network, "edge_weight" in covariate_names     
#
		except: 
			print("Error ocurred during graph initiation.")
			return [], False


