import numpy as np
import graph_tool as gt
import graph_tool.inference as gti
import multiprocessing as mp
from functools import partial


#despite this function being only used within the other, Python need it outside, otherwise it cannot be pickled by partial
def _spur_edge_prob(SBM, edgelist):
	#this function computes the spurious edge probabilities for all single edges in edgelist
	return([(e[0],e[1],SBM.get_edges_prob(missing=[],spurious=[(e[0],e[1])])) for e in edgelist])

#despite this function being only used within the other, Python need it outside, otherwise it cannot be pickled by partial
def _miss_edge_prob(SBM, edgelist):
	#this function computes the missing edge probabilities for all single edges in edgelist
	return([(e[0],e[1],SBM.get_edges_prob(missing=[e],spurious=[])) for e in edgelist])


def compute_single_edge_prob(SBM_in, compute_all_spurious=True, compute_all_missing=True, disconn_nodes=[],miss_edge_list=[],spur_edge_list=[],multiprocess_nodecount=1,maxedges_perjob=100000):
	"""
	Function to compute the single edge confidence scores (edge probabilities) of a stochastic block model using 
	multiprocessing. We do not consider weighted models here!
	

	:Parameters:

	- SBM_in: an SBM for which to compute the edge probabilities
	- compute_all_spurious: Boolean, if edge probabilities for all spurious links should be computed
	- compute_all_missing: Boolean, if edge probabilities for all missing links should be computed
	- disconn_nodes: list of node indices of disconnected nodes. 
	  These are important for computation of missing edge probabilities;
	  the last one will be used to compute all missing edge probabilities with connected nodes, 
	  the last and the first for the edge probability for edges connecting disconnected nodes (all the same!). 
	  Note that for graphs read in via read_graph() and giving the total number of nodes, the disconnected nodes will 
	  be those with largest index.
	- miss_edge_list: only needed if compute_spurious==False (if ==True, all possible missing edges are used),
	  list of putatively missing edges for which to determine the missing edge probabilities, has always 2 elements:
		
		- miss_edge_list[0] is the list of edges between connected nodes,
		- miss_edge_list[1] is the list of edges between disconnected nodes

	- spur_edge_list: only needed if compute_missing=False (if == True, all existing edges are determined), 
	  list of list with putatively spurious edges for which to determine the spurious edge probabilities as first entry, 
	  second entry for list of edges to disconnected nodes
	- multiprocess_nodecount: number of processes which are spawned, default=1 (no multiprocessing)
	- maxedges_perjob: number of egdes whose probability should be computed per job. If too low, overhead by starting 
	  multiple jobs will be high, if too high, runtime can get high. We used 10000 for networks with 8000+ nodes and 2million+ edges.
	
	:Returns:

	A list of three elements:

		- spurious edge probabilities 
		- missing edge probabilities between connected nodes 
		- missing edge probability between a disconnected and otherwise connected nodes 
		
	Of the third, the last element will be the edge probability between two disconnected nodes. Each of the three elements of the list will be a structured, 
	named numpy array for the corresponding edge type in which the source node index is given ('source', int), the target node index ('target', int)
	and the rounded edge probability ('edgeprob', f4).
	
	:Examples:

	Let sbm_metab be an SBM, e.g. read in from read_SBM.read_SBM()::

		#compute edge scores in chunks of 1000 edges for each computation, 
		#multiprocessing with 4 processes
		edgeprobs_mult = compute_single_edge_prob(sbm_metab, 
			compute_all_spurious=True, 
			compute_all_missing=True, 
			disconn_nodes=[161],multiprocess_nodecount=4,
			maxedges_perjob=1000)
	
	Note: Setting maxedges_perjob to 1000 has just been done here to show the output data structure in case of using chunking of edges.		
	However, for this small example, this does not deliver an advantage in runtime - the overhead of starting a new process for each job will increase 
	the runtime compared to launching only one process to compute all edge scores.
	
	Example in which the scores are computed for a list of edges:
	Let sbm_metab be an SBM, e.g. read in from read_SBM.read_SBM(), and using two lists of missing edges for which to compute the edge probabilities,
	for example generated from the R files to determine missing edge lists::
	
		miss_edge_list_conn = calcedge.read_in_edge_list_from_file("example_files/metab_example_missing_edge_list_connected_nodes.txt")
		miss_edge_list_disconn = calcedge.read_in_edge_list_from_file("example_files/metab_example_missing_edge_list_disconnected_nodes.txt")
		edgeprobs = calcedge.compute_single_edge_prob(sbm_metab, 
			compute_all_spurious=False, 
			compute_all_missing=False, 
			disconn_nodes=[161],
			miss_edge_list=[miss_edge_list_conn,miss_edge_list_disconn],
			multiprocess_nodecount=4,maxedges_perjob=100)



	"""
	
	#output data structure
	edgeprobdt=[('source','>i2'), ('target','>i2'), ('edgeprob','>f2')] #two node indices and the edge probability (rounded!)

	spur_prob=[]
	if compute_all_spurious or len(spur_edge_list)>0:
		#prepare edge list spurious nodes: cut the existing edges into chunks of size maxedges_perjob
		if compute_all_spurious: #then all edges will be computed, default
			edgelistspur = [SBM_in.g.get_edges()[i:i + maxedges_perjob] for i in range(0, len(SBM_in.g.get_edges()), maxedges_perjob)] 
		else: #if the spur_edge_list gives some edges
			edgelistspur = [spur_edge_list[i:i + maxedges_perjob] for i in range(0, len(spur_edge_list), maxedges_perjob)] 

		
		def det_spur_edge_prob(SBM, edgelisttable, poolsize=1):
			#determine edge probabilities from edgelisttable (list of edge lists), each edge list will be computed separately
			p = mp.Pool(processes = poolsize)
			func = partial(_spur_edge_prob, SBM)
			edgeproblist = p.map(func, edgelisttable)
			return(edgeproblist)
		
		spur_prob = det_spur_edge_prob(SBM_in,edgelistspur,poolsize=multiprocess_nodecount)
		#re-assemble the data (removing the structure given by chunking) to named, structure nparray
		spur_prob =np.hstack([np.array(spur_prob[i], dtype=edgeprobdt) for i in range(len(edgelistspur))])	
	
	#for missing nodes
	miss_prob_conn=[]
	miss_prob_disconn =[]
	edgelistmissconn=[] #initialize list of missing edges (for connected nodes)
	edgelistmissdisconn=[] #initialize list of missing edges (for disconnected nodes)

	if compute_all_missing or len(miss_edge_list)>0:
		if compute_all_missing: #then we need to find a list of all edges
			#prepare the list of missing edges among the interconnected nodes
			conn_nodes = SBM_in.g.get_vertices()[np.isin(element=SBM_in.g.get_vertices(), test_elements=disconn_nodes,invert=True)] # all connected nodes
			#i.e. the nodes not in the disconnected list
			#maximal index we have in our node list of not disconnected nodes
			maxconnind=max(conn_nodes)
			ind_ar = np.zeros(shape=(int(maxconnind+1),int(maxconnind+1))) #we have to fill this until the maximal index we have in our node list of not disconnected nodes
			rowinds = SBM_in.g.get_edges()[:,0] #rowindices of existing edges
			colinds = SBM_in.g.get_edges()[:,1] #colindices of existing edges
			ind_ar[rowinds,colinds] = -1 #set entries to -one for existing edges
			ind_ar = ind_ar+ind_ar.T #add transpose to copy all to upper triangle
			#new from version 180923 - removed an error
			ind_ar = ind_ar + 1 #set all -ones to 0, all zeros to +1
			ind_ar = np.triu(ind_ar,k=1) #set lower triangle to zeros in order not to count edges twice
		
			np.fill_diagonal(ind_ar, 0, wrap=False)#set diagonal to 0 (we do not want self edges)
			miss_edges = np.where(ind_ar>0.5) # find indices which are not 0
			#remove edge where we have any disconnected node in 
			ind0=np.where(np.isin(element=miss_edges[0],test_elements=disconn_nodes,invert=True)*
			np.isin(element=miss_edges[1],test_elements=disconn_nodes, invert=True)) #only edges which do not contain any disconnected node 
			#(these will not occur in the list of existing edges, but we compute their probability separately)
			miss_edges = list(zip(miss_edges[0][ind0],miss_edges[1][ind0])) #zip them to edge tuples
		
			#cut the list of missing edges into chunks of the maximally allowed edge count
			edgelistmissconn = [miss_edges[i:i + maxedges_perjob] for i in range(0, len(miss_edges), maxedges_perjob)]
		
			#define the list of missing edges from or to disconnected nodes
			if len(disconn_nodes)>0: #if there are disconnected nodes
				#edge list of all edges between any connected nodes (and the first disconnected node) and the last disconnected node
				edgelistmissdc=list(zip(np.array(np.append(conn_nodes,disconn_nodes[0]),dtype=int),
					np.full(len(conn_nodes)+1,disconn_nodes[len(disconn_nodes)-1])))
				#cut in chunks
				edgelistmissdisconn = [edgelistmissdc[i:i + maxedges_perjob] for i in range(0, len(edgelistmissdc), maxedges_perjob)]
		
		else: #if we have a list of missing edges - these would always contain 2 elements
			#cut the list of missing edges into chunks of the maximally allowed edge count
			#edge list for edges between connected nodes
			edgelistmissconn = [miss_edge_list[0][i:i + maxedges_perjob] for i in range(0, len(miss_edge_list[0]), maxedges_perjob)]
			#edge list for edges between disconnected nodes
			edgelistmissdisconn = [miss_edge_list[1][i:i + maxedges_perjob] for i in range(0, len(miss_edge_list[1]), maxedges_perjob)]

		#now compute the edge probabilities for the missing edges from the lists
		def det_miss_edge_prob(SBM, edgelisttable, poolsize=1):
			#determine edge probabilities from edgelisttable (list of edge lists), each edge list will be computed separately
			p = mp.Pool(processes = poolsize)
			func = partial(_miss_edge_prob, SBM)
			edgeproblist = p.map(func, edgelisttable)
			return(edgeproblist)
		#
		miss_prob_conn =  det_miss_edge_prob(SBM_in,edgelistmissconn,poolsize=multiprocess_nodecount)
		#re-arrange to remove chunking structure
		miss_prob_conn =np.hstack([np.array(miss_prob_conn[i], dtype=edgeprobdt) for i in range(len(edgelistmissconn))])
		#
		if len(edgelistmissdisconn)>0: #if there are edges between disconnected nodes for which to determine the probability
			miss_prob_disconn =  det_miss_edge_prob(SBM_in,edgelistmissdisconn,poolsize=multiprocess_nodecount)		
			miss_prob_disconn =  np.hstack([np.array(miss_prob_disconn[i], dtype=edgeprobdt) for i in range(len(edgelistmissdisconn))])
		

	return([spur_prob,miss_prob_conn,miss_prob_disconn])


def determine_spur_edge_list(SBM_in,edge_covcrit):
	"""
	Function to determine the list of existing edges for which to compute edge confidence scores.
	
	:Parameters:

	- SBM_in: SBM of which to determine the edges
	- edge_covcrit: dict containing 
		- 'method': one of 
		
			- 'all' (all spurious edges - default), 
			- 'threshold' (up to a certain threshold of some other variable)
			- 'edge_count' (up to a certain number of edges)	

		- if 'method'=='all' (this is the default), no further info required
		- if method=='threshold', we need two more fields 

	  		- 'threshold_values' giving a vector of the values at which to set the threshold (in order of edges of SBM), and
			- 'cut_off' giving the actual cut-off (edges with threshold_values bigger than cut_off are used)

		- if method=='edge_count', we need two more fields 

			- 'threshold_values' giving a vector of the values which to use to rank the edges, and
			- 'edge_count_value': an integer giving the number of edges to export (the edges with highest values are used)
	
	:Returns:

	An edge list which can be used as input parameter 'spur_edge_list' to the function compute_single_edge_prob()	



	"""

	if edge_covcrit == None: #default is exporting all existing edges
		edge_covcrit={'method':'all'} 

	if edge_covcrit['method']=='all': # if all, we just export all edges
		return(SBM_in.g.get_edges())

	if edge_covcrit['method']=='threshold':
		return([SBM_in.g.get_edges()[i] for i in range(len(SBM_in.g.get_edges())) if edge_covcrit['threshold_values'][i]>edge_covcrit['cut_off']])

	if edge_covcrit['method']=='edge_count':
		#use the threshold_values to determine which cut_off to use
		#sorted threshold values in descending order
		##sort in ascending order and take the last edge_count_value elements
		sort_inds = np.argsort(edge_covcrit['threshold_values'])[(len(edge_covcrit['threshold_values'])-method['edge_count_value']):]
		return([SBM_in.g.get_edges()[i] for i in sort_inds])



def read_in_edge_list_from_file(filename):
	"""
	Function to read in a missing edge list as generated from the R script 'determine_missing_edge_lists.R' and
	bring it in Python usable format (zipping).
	
	:Parameters:
	
	- filename: the name of a file in which the node index pairs are assigned
	
	:Returns:
	
	An edge list in the format to use as input for the parameters 'spur_edge_list' or 'miss_edge_list' 
	to the function compute_single_edge_prob()
	
	:Example:
	
	Read in an edge list from a tab-separated file::
	
		miss_edge_list = read_in_edge_list_from_file('181203_test_edgelist_missconn_metab.txt')



	"""

	edgelist=[]
	with open(filename, 'r') as thefile:
		for line in thefile:
			edgelist.append(tuple([int(i) for i in line.strip().split("\t")]))
#
	return(edgelist)









