read_top_mindl <- function(filename_start,filename_end,containing_folder,file_no_list){
  #read the files which contain the best exported fits (ending on "_top_mindl.txt") performed with global_fit and
  #written to files with write_fit_to_file, returns their list. If files are not there, notification is printed on the screen.
  #
  #Input
  #--------------
  #filename_start: starting string of the filename. Usually the same which has been given as "filename" input to write_fit_to_file()
  #filename_end: ending string of the filename. String which might separate the running index from the _top_mindl.txt ending
  #containing folder: folder in which the files to read can be found
  #file_no_list: vector of integers or strings, this would be the running index of the iterations which have been done
  #
  #Output
  #--------------
  #list of vectors obtained from the files
  #
  #Example
  #--------------
  #topmindl_metab_example <- read_top_mindl(filename_start="result_file_metab_dchSBM",
  #                           filename_end= "",
  #                           containing_folder="~/example_files/",
  #                           file_no_list=1:10)
  #
  top_min_dl=list(length(file_no_list))
  runind <- 0
  for (i in file_no_list){
    #try to read the file and append it to the list 
    filname <- paste0(containing_folder,filename_start,i,filename_end,"_top_mindl.txt")
    if (file.exists(filname)) {#in order to skip non-functional files
      runind <- runind+1 
      top_min_dl[[runind]] <- read.table(filname,
                                         sep="\t",header=FALSE)
    } else {#if the file does not exist, we skip
      print(paste0("_top_mindl-file with number ",i," does not exist. Skipped."))
    }
  }
  return(top_min_dl)
}