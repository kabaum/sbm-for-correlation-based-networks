#create edge lists from correlation data 

write_edge_list_from_corr_pval <- function(load_pval_filename,load_corr_filename, edge_list_filename,multtest_cor_method='BH',sig_thres=0.05){
  #write an edge list into a csv file which can be used as input to graph_tool-based analyses. Basis: significance of correlation.
  #
  #INPUT:
  #-----------
  #load_pval_filename: name of the file where the p-values are found, mandatory, should be an rds file
  #load_corr_filename: name of correlation value file, mandatory
  #edge_list_filename: name of file into which the edge list is written 
  #multtest_cor_method: method of multiple testing correction, default: Benjamini-Hochberg, handed to p.adjust
  #sig_thres: FDR threshold at which to cut, default: 0.05
  #
  #Output
  #------------
  # Prints the number of exported edges on screen and writes a file, no return value.
  #
  #Example
  #------------
  #write_edge_list_from_corr_pval('example_files/180502_metabbud_erneg_spear_pval.rds', 'example_files/180502_metabbud_erneg_spear_corr.rds',
  #edge_list_filename='180507_metabbud_erneg_spear_BH_005_el.csv',
  #multtest_cor_method='BH',
  #sig_thres=0.05)
  
  pval_tab <- readRDS(load_pval_filename) #load p-values
  #make correction
  list_padj <- p.adjust(pval_tab[upper.tri(pval_tab,diag=FALSE)],method=multtest_cor_method)
  mat_padj <- matrix(NA, nrow=dim(pval_tab)[[1]],ncol=dim(pval_tab)[[2]])
  mat_padj[upper.tri(pval_tab)]=list_padj
  
  #produce edge list from that (function from 180212_graph_from_csv.R)
  adj_mat_to_edge_list = function(adj_mat,use_entries_mat){
    #adj_mat: the adjacency matrix (needed for the weights)
    #use_entries_mat: the matrix with Boolean entries at the places which should be used from the adjacency matrix
    adj_list_2 <- sapply(1:dim(use_entries_mat)[[1]],function(x){
      return((0:(dim(use_entries_mat)[[1]]-1))[use_entries_mat[x,]])
    })
    adj_list_1 <- sapply(1:dim(use_entries_mat)[[1]],function(x){
      return(rep(x-1,dim(use_entries_mat)[[1]])[use_entries_mat[x,]])
    })
    adj_list_order <- 0:(length(unlist(adj_list_2))-1)
    adj_list_weight <- sapply(1:dim(use_entries_mat)[[1]],function(x){
      return((adj_mat[x,])[use_entries_mat[x,]])
    })
    
    return(data.frame(source_node = unlist(adj_list_1),
                      target_node = unlist(adj_list_2),
                      edge_ind = adj_list_order,
                      edge_weight = unlist(adj_list_weight)))
  }                              
  #apply threshold on the fly!!!
  edge_list <- adj_mat_to_edge_list(readRDS(load_corr_filename),!is.na(mat_padj) & mat_padj<sig_thres)
  print(dim(edge_list)) # for chekcing purposes show size of edge list file!!!
  write.table(edge_list,edge_list_filename,sep=",",row.names=FALSE)
}




write_edge_list_from_corr_thres <- function(load_corr_filename, corr_thres, edge_list_filename){
  #create an edge list csv file which can be used further as input to graph_tool-based analyses given the correlation matrix and a threshold. Only
  #edges of which the absolute correlation is larger than the threshold are going to be written to the edge list.
  #
  #Input
  #----------
  #load_corr_filename: filename in which the correlation matrix is to be found (symmetric). Expected to be an .rds file
  #corr_thres: float value, threshold to use (abs(corr)> corr_thres)
  #edge_list_filename: filename into which to write the edge list
  #
  #Output
  #-----------
  #None. Writes edges list from correlation threshold into a file, prints number of exported edges to the screen.
  #
  #Example
  #-----------
  #write_edge_list_from_corr_thres(load_corr_filename='180502_metabbud_erneg_spear_corr.rds',
  #corr_thres=0.375,
  #edge_list_filename='180702_metabbud_erneg_spear_scalefree_el.csv')
  #
  adj_mat_to_edge_list = function(adj_mat,use_entries_mat){
    #adj_mat: the adjacency matrix (needed for the weights)
    #use_entries_mat: the matrix with Boolean entries at the places which should be used from the adjacency matrix
    adj_list_2 <- sapply(1:dim(use_entries_mat)[[1]],function(x){
      return((0:(dim(use_entries_mat)[[1]]-1))[use_entries_mat[x,]])
    })
    adj_list_1 <- sapply(1:dim(use_entries_mat)[[1]],function(x){
      return(rep(x-1,dim(use_entries_mat)[[1]])[use_entries_mat[x,]])
    })
    adj_list_order <- 0:(length(unlist(adj_list_2))-1)
    adj_list_weight <- sapply(1:dim(use_entries_mat)[[1]],function(x){
      return((adj_mat[x,])[use_entries_mat[x,]])
    })
    return(data.frame(source_node = unlist(adj_list_1),
                      target_node = unlist(adj_list_2),
                      edge_ind = adj_list_order,
                      edge_weight = unlist(adj_list_weight)))
  }  
  #save upper triangle only
  corr_tab <- readRDS(load_corr_filename)
  mat_padj <- matrix(NA, nrow=dim(corr_tab)[[1]],ncol=dim(corr_tab)[[2]])
  mat_padj[upper.tri(corr_tab)]=1
  #apply threshold on the fly
  edge_list <- adj_mat_to_edge_list(corr_tab,!is.na(mat_padj) & abs(corr_tab)>corr_thres)
  print(dim(edge_list)) # for checking purposes show size of edge list file!!!
  write.table(edge_list,edge_list_filename,sep=",",row.names=FALSE)
}