
#this is for only computing missing edges using given files
import graph_tool as gt # only for setting one thread at a time
import calc_edge_prob as calcedge
import read_SBM as rsbm
import numpy as np
import sys


def main():
	el_name = sys.argv[1] #name of edge list to import
	total_node_count = sys.argv[2] #add nodes such that the graph has that number od nodes
	start_blockfile = sys.argv[3] #start of filename of partition file
	end_blockfile = sys.argv[4] #end of filename of partition file
	level_count = sys.argv[5] #number of hierarchy levels (i.e. count of files)
	is_hier = sys.argv[6] #if it is hierarchical or not (1 or 0)
	is_dc = sys.argv[7] #if it is degree corrected or not (1 or 0)
	conn_node_count = sys.argv[8] #number of connected nodes (i.e. in the reduced network) 
	out_name = sys.argv[9] #name of the output file, given by shell script in order to write to distinct files
	compnode_count = sys.argv[10] #how many jobs are allowed to run in parallel
	edgecount_perjob = sys.argv[11] #how many edges should be computed per job
	filename_miss_list_conn = sys.argv[12] #name of the list of missing edges between connected nodes, file needs to be provided
	filename_miss_list_disconn=sys.argv[13] #name of the list of missing edges from a disconnected node, file needs to be provided


	gt.openmp_set_num_threads(1) #use only one core at a time

	#initialize sbm
	sbm = rsbm.read_SBM(el_name,int(total_node_count), 
		start_blockfile,end_blockfile,
		no_hier_levels = int(level_count),
		is_hierarchical=bool(int(is_hier)),
		is_degree_corrected=bool(int(is_dc)))
	
	#read in the lists of missing edges
	miss_edge_list_conn = calcedge.read_in_edge_list_from_file(filename_miss_list_conn)
	miss_edge_list_disconn = calcedge.read_in_edge_list_from_file(filename_miss_list_disconn)
	
	#compute edge probabilities
	edgeprobs = calcedge.compute_single_edge_prob(sbm, 
		compute_all_spurious=False, 
		compute_all_missing=False, 
		disconn_nodes=range(int(conn_node_count),int(total_node_count)),
		miss_edge_list=[miss_edge_list_conn,miss_edge_list_disconn],
		multiprocess_nodecount=int(compnode_count),maxedges_perjob=int(edgecount_perjob))

	#np.save(file=out_name+"spur_edge_prob.npy", arr=edgeprobs[0]) 
	np.save(file=out_name+"miss_connedge_prob.npy", arr=edgeprobs[1]) 
	np.save(file=out_name+"miss_disconnedge_prob.npy", arr=edgeprobs[2]) 

if __name__ == '__main__': #call the script if this calls the function, not if only loaded
	main()
